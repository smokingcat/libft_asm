/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   __main.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/19 15:58:00 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/19 18:43:09 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libfts.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <ctype.h>

int		main(int ac, char **av)
{
	if (ac != 2)
		return (0);
	if (strcmp(av[1], "ft") == 0)
	{
		test_puts(ft_puts);
		test_is(ft_isalnum, "FT_ISALNUM");
		test_is(ft_isascii, "FT_ISASCII");
		test_is(ft_isdigit, "FT_ISDIGIT");
		test_is(ft_isalpha, "FT_ISALPHA");
		test_is(ft_islower, "FT_ISLOWER");
		test_is(ft_isupper, "FT_ISUPPER");
		test_is(ft_isprint, "FT_ISPRINT");
		test_toupper(ft_toupper, "FT_TOUPPER");
		test_tolower(ft_tolower, "FT_TOLOWER");
		test_strlen(ft_strlen, "FT_STRLEN");
		test_bzero(ft_bzero, "FT_BZERO");
		test_strcat(ft_strcat, "FT_STRCAT");
		test_memset(ft_memset, "FT_MEMSET");
		test_memcpy(ft_memcpy, "FT_MEMCPY");
		test_strdup(ft_strdup, "FT_STRDUP");
	}
	else
	{
		test_puts(puts);
		test_is(isalnum, "FT_ISALNUM");
		test_is(isascii, "FT_ISASCII");
		test_is(isdigit, "FT_ISDIGIT");
		test_is(isalpha, "FT_ISALPHA");
		test_is(islower, "FT_ISLOWER");
		test_is(isupper, "FT_ISUPPER");
		test_is(isprint, "FT_ISPRINT");
		test_toupper(toupper, "FT_TOUPPER");
		test_tolower(tolower, "FT_TOLOWER");
		test_strlen(strlen, "FT_STRLEN");
		test_bzero(bzero, "FT_BZERO");
		test_strcat(strcat, "FT_STRCAT");
		test_memset(memset, "FT_MEMSET");
		test_memcpy(memcpy, "FT_MEMCPY");
		test_strdup(strdup, "FT_STRDUP");
	}
	return (0);
}

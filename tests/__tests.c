/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   __tests.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/19 16:02:56 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/23 15:08:23 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libfts.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <ctype.h>

void	test_is(int (*fun)(int), char *msg)
{
	int i;
	int ret;

	printf("%s\n", msg);
	printf("%s\n", "==========");
	i = 0;
	while (i++ <= 127)
	{
		ret = fun(i);
		printf("%d ", ret);
	}
	printf("\n");
}

void	test_toupper(int (*fun)(int), char *msg)
{
	int i;
	int ret;

	printf("%s\n", msg);
	printf("%s\n", "==========");
	i = 0;
	while (i++ <= 127)
	{
		ret = fun(i);
		printf("%d ", ret);
	}
	printf("\n");
}

void	test_tolower(int (*fun)(int), char *msg)
{
	int i;
	int ret;

	printf("%s\n", msg);
	printf("%s\n", "==========");
	i = 0;
	while (i++ <= 127)
	{
		ret = fun(i);
		printf("%d ", ret);
	}
	printf("\n");
}

void	test_strlen(size_t (*fun)(const char *), char *msg)
{
	int ret;
	char **strings = (char *[])
	{
		"",
		"h",
		"hello",
		"long string",
		"another\tweird\ntest",
		0
	};

	printf("%s\n", msg);
	printf("%s\n", "==========");
	while (*strings)
	{
		ret = fun(*strings);
		printf("%d ", ret);
		strings++;
	}
	printf("\n");
}

void	test_bzero(void (*fun)(void *, size_t), char *msg)
{
	int		i;
	int 	len;
	char	*str;
	char	**strings = (char *[])
	{
		"",
		"h",
		"hello",
		"long string",
		"another\tweird\ntest",
		0
	};

	printf("%s\n", msg);
	printf("%s\n", "==========");

	while (*strings)
	{
		i = 0;

		str = strdup(*strings);
		len = strlen(str);
		fun(str, 4);
		while (i <= len)
		{
			if (str[i])
				printf("%c", str[i]);
			else
				printf("\\0");
			i++;
		}
		printf("\n");
		strings++;
	}
}

void	test_puts(int (*fun)(const char *))
{
	char **strings = (char *[])
	{
		"",
		"h",
		"hello",
		"long string",
		"another\tweird\ntest",
		0
	};

	while(*strings)
	{
		fun(*strings);
		strings++;
	}
}

void	test_strcat(char *(*fun)(char *, const char *), char *msg)
{
	char *buf;

	buf = malloc(10000);
	char **strings = (char *[])
	{
		"",
		"h",
		"hello",
		"long string",
		0
	};

	char **strings2 = (char *[])
	{
		"long string",
		"hello",
		"h",
		"",
		0
	};

	printf("%s\n", msg);
	printf("%s\n", "==========");
	while (*strings)
	{
		bzero(buf, 1000);
		buf = strcpy(buf, *strings);
		buf = fun(buf, *strings2);
		printf("%s\n", buf);
		strings++;
		strings2++;
	}
}

void	test_memset(void *(*fun)(void *, int c, size_t), char *msg)
{
	char str[500];

	printf("%s\n", msg);
	printf("%s\n", "==========");

	strcpy(str, "This is string.h library function");
	printf("%s\n", str);

	int i = 0;
	char c = 'a';
	while (i++ <= 30)
	{
		fun(str, c++, i);
		printf("%s\n", str);
	}
}

void	test_memcpy(void *(*fun)(void *, const void *, size_t), char *msg)
{
	const char 	src[50] = "http://www.google.com/";
   	char 		dest[50];

	printf("%s\n", msg);
	printf("%s\n", "==========");

	ft_bzero(dest, 50);
	printf("before memcpy dest = %s\n", dest);
	fun(dest, src, strlen(src) + 1);
	printf("after  memcpy dest = %s\n", dest);

}

void	test_strdup(char *(*fun)(const char *), char *msg)
{
	const char 	*src;
	char 		*dest;

	src = strdup("http://www.google.com/");
	printf("%s\n", msg);
	printf("%s\n", "==========");

	dest = fun(src);
	printf("%s\n", dest);
}

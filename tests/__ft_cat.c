/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   __ft_cat.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/18 14:53:44 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/19 18:46:57 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libfts.h"
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

int main(int ac, char **av)
{
	int fd;

	if (!av[1])
	{
		ft_cat(0);
	}
	else
	{
		av++;
		while(*av)
		{
			fd = open (*av, O_RDONLY);
			ft_cat(fd);
			close(fd);
			av++;
		}
	}
	return (ac);
}

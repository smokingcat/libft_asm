# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_isupper.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/14 11:00:03 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/14 14:25:04 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_isupper

_ft_isupper:
	mov rax, 0
	cmp rdi, 91
	jl .jumpone
	ret

.jumpone:
	mov rax, 1
	cmp rdi, 65
	jl .jumpzero
	ret

.jumpzero:
	mov rax, 0
	ret

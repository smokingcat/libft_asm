# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strcat.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/16 13:42:46 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/19 15:26:01 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_strcat
	extern _ft_strlen
	extern _ft_memcpy

_ft_strcat:
	push rdi
	call _ft_strlen
	add rdi, rax
	xchg rdi, rsi
	call _ft_strlen
	xchg rdi, rsi
	mov rcx, rax
	rep movsb
	pop rax
	ret

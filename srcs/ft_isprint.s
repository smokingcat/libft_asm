# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_isprint.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/14 13:35:02 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/14 13:35:53 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_isprint

_ft_isprint:
	mov rax, 0
	cmp rdi, 127
	jl .jumpone
	ret

.jumpone:
	mov rax, 1
	cmp rdi, 32
	jl .jumpzero
	ret

.jumpzero:
	mov rax, 0
	ret

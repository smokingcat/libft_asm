# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_cat.s                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/18 14:34:04 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/19 15:32:24 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

%define READ 0x2000003
%define WRITE 0x2000004
%define STDOUT 1
%define STDIN 0

section .text
	global _ft_cat

section .data
buf times 1 db 0
bufsize equ $ - buf

segment .text

_ft_cat:
	push rdi

loop:
	pop rdi
	lea rsi, [rel buf]
	mov rdx, bufsize
	mov rax, READ
	syscall
	jc end
	cmp rax, 0
	jle end
	push rdi
	mov rdi, STDOUT
	lea rsi, [rel buf]
	mov rdx, bufsize
	mov rax, WRITE
	syscall
	jc end
	jmp loop

end:
	ret

; rax: syscall WRITE | OPEN
; FOR WRITE:
; rsi: string | rdx: len(str) | rax: file descriptor (1)
; FOR READ
; rax : file descriptor (3)

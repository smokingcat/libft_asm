# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_memset.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/17 15:07:38 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/17 15:54:54 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


section .text
	global _ft_memset

_ft_memset:
	push rdi
	mov rax, rsi
	mov rcx, rdx
	cld
	rep stosb
	pop rdi
	mov rax, rdi
	ret

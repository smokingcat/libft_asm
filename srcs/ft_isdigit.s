# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_isdigit.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/13 19:06:00 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/17 16:24:15 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_isdigit

_ft_isdigit:
	mov rax, 0
	cmp rdi, 58
	jl .jumpone
	ret

.jumpone:
	mov rax, 1
	cmp rdi, 48
	jl .jumpzero
	ret

.jumpzero:
	mov rax, 0
	ret

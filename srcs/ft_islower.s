# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_islower.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/14 11:08:06 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/14 14:23:35 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_islower

_ft_islower:
	mov rax, 0
	cmp rdi, 123
	jl .jumpone
	ret

.jumpone:
	mov rax, 1
	cmp rdi, 97
	jl .jumpzero
	ret

.jumpzero:
	mov rax, 0
	ret

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_puts.s                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/17 11:29:35 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/17 15:32:14 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

%define WRITE 0x2000004
%define STDOUT 1

section .text
	global _ft_puts
	extern _ft_strlen


; WRITE: rax: syscall | rsi: string | rdx: len(str) | rdi: file descriptor

_ft_puts:
	cmp rdi, 0
	jle .null
	call _ft_strlen
	mov rdx, rax		; put strlen under rdx
	mov rsi, rdi		; set write argument to string
	mov rdi, STDOUT		; file descriptor
	mov rax, WRITE		; put sys_write (4) under rax
	syscall				; kernell call
	jmp .newline
	ret

.newline:
	mov rdx, 1
	lea rsi, [rel new_line]
	mov rdi, STDOUT
	mov rax, WRITE
	syscall
	ret

.null:
	mov rdx, null.len
	lea rsi, [rel null]
	mov rdi, STDOUT
	mov rax, WRITE
	syscall
	ret

section .data

new_line db `\n`
null db "(null)", `\n`
.len equ $ - null


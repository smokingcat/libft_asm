# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_bzero.s                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/16 11:10:09 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/17 16:12:14 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_bzero
	extern _ft_strlen
	extern _ft_memset

_ft_bzero:
	push rdi
	mov rdx, rsi
	mov rsi, 0
	call _ft_memset
	pop rdi
	ret

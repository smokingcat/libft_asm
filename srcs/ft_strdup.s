# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strdup.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/17 16:30:53 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/17 18:23:33 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_strdup
	extern _ft_strlen
	extern _ft_memcpy
	extern _malloc

_ft_strdup:

	push rdi
	push rsi
	push rdx

	mov r11, rdi
	call _ft_strlen

	mov r12, rax
	mov rdi, rax
	inc rdi
	push r11
	call _malloc
	pop rsi

	mov rdi, rax
	mov rdx, r12
	call _ft_memcpy

	pop rdx
	pop rsi
	pop rdi
	ret

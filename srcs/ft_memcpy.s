# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_memcpy.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/17 10:09:10 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/17 18:14:28 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_memcpy

_ft_memcpy:
	push rdi
	push rsi
	mov rcx, rdx
	cld
	repnz movsb
	pop rsi
	pop rdi
	mov rax, rdi
	ret

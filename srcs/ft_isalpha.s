# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_isalpha.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/14 10:43:20 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/14 12:37:17 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_isalpha
	extern _ft_islower
	extern _ft_isupper

_ft_isalpha:
	mov rax, 0
	call _ft_islower
	cmp rax, 0
	je .isItUpperThen
	ret

.isItUpperThen:
	call _ft_isupper
	ret

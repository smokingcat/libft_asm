# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_toupper.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/14 13:41:27 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/14 14:25:48 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_toupper
	extern _ft_islower

_ft_toupper:
	call _ft_islower
	cmp rax, 1
	je .ItsLower
	mov rax, rdi
	ret

.ItsLower:
	sub rdi, 32
	mov rax, rdi
	ret

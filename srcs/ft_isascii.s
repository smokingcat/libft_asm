# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_isascii.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/13 19:50:25 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/13 19:54:12 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_isascii

_ft_isascii:
	mov rax, 0
	cmp rdi, 128
	jl .jumpone
	ret

.jumpone:
	mov rax, 1
	cmp rdi, 0
	jl .jumpzero
	ret

.jumpzero:
	mov rax, 0
	ret

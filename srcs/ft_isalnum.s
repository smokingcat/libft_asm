# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_isalnum.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/14 12:37:03 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/14 12:41:00 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_isalnum
	extern _ft_isalpha
	extern _ft_isdigit

_ft_isalnum:
	mov rax, 0
	call _ft_isalpha
	cmp rax, 0
	je .isItNumericThen
	ret

.isItNumericThen:
	call _ft_isdigit
	ret

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strlen.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/17 15:02:27 by vde-chab          #+#    #+#              #
#    Updated: 2015/03/19 15:27:48 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global _ft_strlen

_ft_strlen:
	push rdi
	push rsi
	xor al, al
	xor rcx, rcx
	not rcx
	cld
	mov rsi, rdi
	repnz scasb
	mov rax, rcx
	not rax
	dec rax
	pop rsi
	pop	 rdi
	ret

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libfts.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-chab <vde-chab@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/13 19:17:33 by vde-chab          #+#    #+#             */
/*   Updated: 2015/03/19 18:51:31 by vde-chab         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTS_H
# define LIBFTS_H

/*
** Unlock size_t
*/
# include <string.h>

/*
** PART ONE
*/
int		ft_isdigit(int c);
int		ft_isascii(int c);
int		ft_isalpha(int c);
int		ft_isalnum(int c);
int		ft_isprint(int c);
int		ft_toupper(int c);
int		ft_tolower(int c);
void	ft_bzero(void *s, size_t n);
int		ft_tolower(int c);
char	*ft_strcat(char *s1, const char *s2);
int		ft_puts(const char *s);

/*
** PART TWO
*/
size_t	ft_strlen(const char *s);
void	*ft_memcpy(void *dst, const void *src, size_t n);
void	*ft_memset(void *b, int c, size_t len);
char	*ft_strdup(const char *s1);

/*
** MIAOU PART
*/
void	ft_cat(int fd);

/*
** BONUSES
*/
int		ft_isupper(int c);
int		ft_islower(int c);

/*
** TESTING
*/
void	test_is(int (*fun)(int), char *msg);
void	test_toupper(int (*fun)(int), char *msg);
void	test_tolower(int (*fun)(int), char *msg);
void	test_strlen(size_t (*fun)(const char *), char *msg);
void	test_bzero(void (*fun)(void *, size_t), char *msg);
void	test_puts(int (*fun)(const char *));
void	test_strcat(char *(*fun)(char *, const char *), char *msg);
void	test_memset(void *(*fun)(void *, int c, size_t), char *msg);
void	test_memcpy(void *(*fun)(void *, const void *, size_t), char *msg);
void	test_strdup(char *(*fun)(const char *), char *msg);

#endif

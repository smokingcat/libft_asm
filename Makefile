# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jbailhac <jbailhac@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/03 11:46:23 by jbailhac          #+#    #+#              #
#    Updated: 2015/03/19 18:45:07 by vde-chab         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC			=~/.brew/bin/nasm

CFLAGS		=-f macho64

NAME		=libfts.a

SRC_PATH	=./srcs/

SRC_NAME	=ft_isdigit.s			\
			 ft_isascii.s			\
			 ft_isupper.s			\
			 ft_islower.s			\
			 ft_isalpha.s			\
			 ft_isalnum.s			\
			 ft_isprint.s			\
			 ft_toupper.s			\
			 ft_tolower.s			\
			 ft_strcat.s			\
			 ft_strlen.s			\
			 ft_memcpy.s			\
			 ft_memset.s			\
			 ft_strdup.s			\
			 ft_bzero.s				\
			 ft_puts.s				\
			 ft_cat.s				\

OBJ_PATH	=./objs/

OBJ_NAME	=$(SRC_NAME:.s=.o)

SRC			=$(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ			=$(addprefix $(OBJ_PATH),$(OBJ_NAME))

.PHONY:		all clean fclean re home

all:		$(NAME)

clean:
			@/bin/rm -f $(OBJ)
			@rmdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
			@echo "\x1b[32mobjects cleaned.\x1b[0m"

fclean:		clean
			@/bin/rm -f $(NAME)
			@echo "\x1b[32m$(NAME) cleaned.\x1b[0m"

$(NAME):	$(OBJ)
			@ar -rc $(NAME) $(OBJ)
			@echo "\x1b[32m$(NAME) done.\x1b[0m"

$(OBJ_PATH)%.o: $(SRC_PATH)%.s
			@mkdir $(OBJ_PATH) 2> /dev/null || echo "" > /dev/null
			@$(CC) $(CFLAGS) -o $@ $^
			@echo "$< compiled"

cat:		all
			@gcc -Wall -Wextra -Werror tests/__ft_cat.c libfts.a -o cat

re:			fclean all
